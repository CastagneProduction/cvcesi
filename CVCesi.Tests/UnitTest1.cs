﻿using System;
using System.Collections.Generic;
using CVCesi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CVCesi.Tests
{
    [TestClass]
    public class UnitTest1
    {
        // Attribut dal qui permet d'appeler les méthodes de l'interface IDal
        //private IDal dal;
        
        // ________________________________________________________________________ //
        //                          LISTE DES TESTS                                 //
        // ________________________________________________________________________ //

        [TestMethod]
        public void CreerCandidat_AvecUnNouveauCandidat_ObtientTousLesCandidatsRenvoitBienLeCandidat()
        {
            using (IDal dal = new Dal())
            {
                dal.Creer_Candidat("Simonlatser", "Nicolas", "Nicolas@Nicolas.fr");
                List<Candidat> Candidats = dal.Obtenir_Tous_Candidats();

                Assert.IsNotNull(Candidats);
                Assert.AreEqual(1, Candidats.Count);
                Assert.AreEqual("Simonlatser", Candidats[0].Nom);
                Assert.AreEqual("Nicolas", Candidats[0].Prenom);
                Assert.AreEqual("Nicolas@Nicolas.fr", Candidats[0].Email);
            }
        }
    }
}
