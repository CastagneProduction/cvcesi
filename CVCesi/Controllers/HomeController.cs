﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CVCesi.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Connexion(String submit)
        {
            if (submit == "MEMBRE")
            {
                return View();
            }
            else
            {
                return View("Connexion_candidat");
            }            
        }
    }
}