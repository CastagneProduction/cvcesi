﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVCesi.Models
{
    public class Personnel_CESI
    {
        string Nom { get; set; }
        string Email { get; set; }
        string Mot_De_Passe { get; set; }
        bool Est_Admin { get; set; }
    }
}