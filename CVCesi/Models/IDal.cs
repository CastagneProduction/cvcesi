﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVCesi.Models
{
    public interface IDal : IDisposable
    {
        List<Candidat> Obtenir_Tous_Candidats();
        void Creer_Candidat(string nom, string prenom, string email);
    }
}