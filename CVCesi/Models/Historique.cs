﻿using CVCesi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVCesi.Models
{
    public class Historique
    {
        public string Nom_Acteur{ get; set; }
        public Type_Action Type_Action { get; set; }
        public string Cible_Action { get; set; }
    }
}