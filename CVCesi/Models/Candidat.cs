﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVCesi.Models
{
    public class Candidat
    {
        int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }

        public ICollection<CV> CV { get; set; }
    }
}