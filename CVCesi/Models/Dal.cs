﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace CVCesi.Models
{
    public class Dal : IDal
    {
        private BddContext bdd;

        public Dal()
        {
            bdd = new BddContext(); 
        }

        public List<Candidat> Obtenir_Tous_Candidats()
        {
            return bdd.Candidats.ToList();
        }

        public void Dispose()
        {
            bdd.Dispose();
        }

        public void Creer_Candidat(string nom, string prenom, string email)
        {
            bdd.Candidats.Add(new Candidat { Nom = nom, Prenom = prenom, Email = email });
            bdd.SaveChanges();
        }
    }
}