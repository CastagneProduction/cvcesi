﻿using CVCesi.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CVCesi.Models
{
    public class CV
    {
        string Titre { get; set; }
        public string Chemin { get; set; }
        public Statut_CV Statut { get; set; } 
        
        public Candidat Candidat { get; set; }

    }
}