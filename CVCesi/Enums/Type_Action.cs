﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVCesi.Enums
{
    public enum Type_Action
    {
        Creation,
        Edition,
        Suppression
    }
}